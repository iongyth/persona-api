from flask import Flask, jsonify
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://mongodb:27017/db"
mongo = PyMongo(app)

@app.route('/search/<username>', methods=['GET'])
def search_username(username):
	"""
	Finds a person in the mongo database by name

	:param username: the user to return from mongo
	:return:
	"""
	return jsonify(
		mongo.db.people.find_one_or_404({"username": username}, { "_id": 0 })
	)

@app.route('/people', methods=['GET'])
def search_people():
	"""
	Returns all people
	:return:
	"""
	return jsonify(
		[x for x in mongo.db.people.find({}, { "_id": 0 })]
	)


@app.route('/people/<username>', methods=['DELETE'])
def delete_username(username):
	"""
	Removes a person from mongo

	:param username: the username to remove from mongo
	:return:
	"""
	mongo.db.people.delete_one({"username": username})
	return 'User successfully deleted'


if __name__ == '__main__':
	app.run(host='0.0.0.0')

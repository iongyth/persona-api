FROM python:2.7-alpine

RUN mkdir -p /vol/persona-api
WORKDIR /vol/persona-api

COPY requirements.txt /vol/persona-api

RUN pip install --no-cache-dir -r requirements.txt

COPY . /vol/persona-api

EXPOSE 5000

ENTRYPOINT ["python"]

CMD ["api/handlers.py"]
